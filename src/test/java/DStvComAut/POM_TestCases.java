package DStvComAut;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import pageObjects.channelsPage;
import pageObjects.homePage;

import java.util.concurrent.TimeUnit;

/**
 * Created by Lindo on 2017-06-05.
 */
public class POM_TestCases {

    private static WebDriver driver = null;

    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C://chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("http://staging-guide.dstv.com");
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        homePage.Search(driver).sendKeys("Talk");
        homePage.startSearch(driver).click();
        homePage.returnToGuide(driver).click();
        Thread.sleep(30000);
        homePage.verifyPageTitle(driver);
        //homePage.OpenSchedule(driver).click();
        //homePage.CloseSchedule(driver).click();

        //homePage.changePackage(driver).click();

        //channelsPage.openFilters(driver).click();
        //channelsPage.channelsLanding(driver).click();
        //channelsPage.verifyChannelsPage(driver).click();

        System.out.println("Successful");

        driver.close();



    }
}

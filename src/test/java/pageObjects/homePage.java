package pageObjects;

import com.thoughtworks.selenium.webdriven.commands.Click;
import org.apache.bcel.generic.Select;
import org.apache.bcel.generic.Visitor;
import org.apache.http.util.Asserts;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.w3c.dom.html.HTMLSelectElement;
import org.testng.Assert;


/**
 * Created by Lindo on 2017-06-05.
 */
public class homePage {

    private static WebElement element = null;

    public static WebElement OpenSchedule(WebDriver driver){

        element = driver.findElement(By.xpath(".//*[@id='channels-Load']/div[4]/div/div[18]/div/ul[1]/li[1]"));
        return element;

    }

    public static WebElement CloseSchedule(WebDriver driver){
        element = driver.findElement(By.xpath(".//*[@id='popup-modal']/div/div/div[1]/button"));
        return element;
    }

    public static WebElement Search(WebDriver driver){
        element = driver.findElement(By.xpath(".//*[@id='searchTerm-local']"));
        return element;
    }

    public static WebElement startSearch(WebDriver driver){
        element = driver.findElement(By.xpath(".//*[@id='searchbtn-local']"));
        return element;
    }
    public static WebElement returnToGuide(WebDriver driver) throws InterruptedException {
        element = driver.findElement(By.xpath(".//*[@id='backButton']/p"));
        Thread.sleep(5000);
        return element;
    }

    public static WebElement changePackage(WebDriver driver){

        element = driver.findElement(By.xpath(".//*[@id='bouquetdropdownTp']"));
        element = driver.findElement(By.name("DStv Extra"));
        return element;
    }

    public static void  verifyPageTitle(WebDriver driver){

       Assert.assertEquals("DStv Guide", driver.getTitle());


    }


}

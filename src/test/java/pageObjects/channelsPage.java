package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by Lindokuhle.Ngobese on 2017/06/07.
 */
public class channelsPage {

    private static WebElement element = null;

    public static WebElement openFilters(WebDriver driver){
        element = driver.findElement(By.className("filter-label"));
        return element;
    }

    public static WebElement channelsLanding(WebDriver driver){
        element = driver.findElement(By.id("page-channels"));
        return element;
    }

    public static WebElement verifyChannelsPage(WebDriver driver){
        element = driver.findElement(By.id("bouquetdropdown"));
        return element;
    }


}
